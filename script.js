const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];
let divRoot = document.getElementById("root");
let ulRoot = document.createElement("ul");

function rozbiv({ author, name, price }) {
  return ` ${author} - ${name} - ${price} `;
}

divRoot.append(ulRoot);
books.forEach((el) => {
  let liRoot = document.createElement("li");
  try {
    
  if (!el.author) {
      throw new Error(`Отсутствует поле "author"`);
  }
  
  if (!el.name) {
      throw new Error(`Отсутствует поле "name"`);
  }
  
  if (!el.price) {
      throw new Error(`Отсутствует поле "price"`);
  }

    liRoot.append(rozbiv(el));
    ulRoot.append(liRoot);
  } catch (err) {
    console.log(err);
  }
});
